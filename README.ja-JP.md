## 概要

madong-vbenは、vben5に基づくフロントエンドエンジニアリングの迅速な開発フレームワークです。

#### 特徴

- **最新の技術スタック**：Vue3やViteなどの最先端のフロントエンド技術を使用して開発されています。
- **TypeScript**：アプリケーションレベルのJavaScriptのための言語です。
- **テーマ**：複数のテーマカラーを提供し、カスタマイズ可能なオプションがあります。
- **国際化**：包括的な国際化サポートが内蔵されています。
- **権限管理**：堅牢な動的ルーティング権限生成スキームが備わっています。

#### プレビュー

- [antd-vben5](http://antd-vben5.madong.tech/) - 完全版の日本語サイト

テストアカウント: admin/123456

#### ドキュメント

[ドキュメントリンク](https://doc.vben.pro/)

#### コードリポジトリ
- Gitee リポジトリ: [https://gitee.com/motion-code/madong-vben5](https://gitee.com/motion-code/madong-vben5)
- Gitcode リポジトリ: [https://gitcode.com/motion-code/madong-vben5](https://gitcode.com/motion-code/madong-vben5)

#### バックエンドリポジトリ
- Gitee リポジトリ: [https://gitee.com/motion-code/madong](https://gitee.com/motion-code/madong)
- Gitcode リポジトリ: [https://gitcode.com/motion-code/madong](https://gitcode.com/motion-code/madong)

#### インストールと使用方法

- 依存関係をインストールする

```bash
cd madong-vben5

corepack enable

pnpm install
```

- 実行する

```bash
pnpm dev
```

- ビルドする

```bash
pnpm build
```

#### 更新履歴

[CHANGELOG](https://gitee.com/motion-code/madong-vben5/commits/master)

#### 参加方法

あなたの参加を心より歓迎します！[Issueを提起する](https://gitee.com/motion-code/madong-vben5/issues)か、Pull Requestを提出してください。

**Pull Request:**

1. リポジトリをフォークしてください！
2. 自分のブランチを作成します: `git checkout -b feature/xxxx`
3. 変更をコミットします: `git commit -am 'feat(function): add xxxxx'`
4. ブランチをプッシュします: `git push origin feature/xxxx`
5. Pull Requestを提出します

#### Git貢献コミット規約

- [vue](https://github.com/vuejs/vue/blob/dev/.github/COMMIT_CONVENTION.md)の規約を参考にしてください（[Angular](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-angular)）

  - `feat` 新機能の追加
  - `fix` バグ修正
  - `style` 実行結果に影響しないコードスタイル関連
  - `perf` パフォーマンスの向上
  - `refactor` コードのリファクタリング
  - `revert` 変更を元に戻す
  - `test` テスト関連
  - `docs` ドキュメント/コメント
  - `chore` 依存関係の更新/スクリプト設定の変更など
  - `ci` 継続的インテグレーション
  - `types` 型定義ファイルの変更
  - `wip` 作業中

#### ブラウザサポート

ローカル開発では、`Chrome 80+`の使用を推奨します。

最新のブラウザをサポートし、IEはサポートしていません。

| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt=" Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>IE | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt=" Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Edge | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari |
| :-: | :-: | :-: | :-: | :-: |
| サポートなし | 最新2バージョン | 最新2バージョン | 最新2バージョン | 最新2バージョン |

#### お問い合わせ

オープンソース技術に興味がある方や、コラボレーションを希望される方は、以下の方法でお気軽にご連絡ください。

- **チャンネル**：[pd52261144](https://pd.qq.com/s/3edfwx2lm)
- **メール**：[405784684@qq.com]
- **コミュニティ**：[https://madong.tech](https://madong.tech)

#### メンテナー

[Mr.April](https://gitee.com/liu_guan_qing)
