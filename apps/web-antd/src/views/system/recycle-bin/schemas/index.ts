import type { VbenFormProps, VxeGridProps } from '#/adapter';
// 表格参数定义
export const gridSchemas: VxeGridProps<any> = {
  toolbarConfig: {
    refresh: true, // 刷新
    print: false, // 打印
    export: false, // 导出
    // custom: true, // 自定义列
    zoom: true, // 最大化最小化
  },
  columnConfig: {
    resizable: true,//开启拖拽行宽模式
  },
  columns: [
    { type: 'checkbox', width: 60 },
    { field: 'table_name', title: '数据表',align:'center'},
    { field: 'data', title: '数据'},
    { field: 'ip', title: 'IP' },
    {
      field: 'enabled',
      title: '是否还原',
      width:90,
      cellRender: {
        name: 'ApiDict',
        props: {
          code: 'yes_no',
        },
      },
    },
    {
      width: 200,
      title: '操作',
      align: 'center',
      slots: { default: 'ACTION' },
      fixed: 'right',
    },
  ],
};
// 搜索表单参数定义
export const searchFormSchemas: VbenFormProps = {
  schema: [
    {
      component: 'Input',
      fieldName: 'LIKE_table_name',
      label: '数据表名称',
      componentProps: {
        placeholder: '请输入表名称',
        allowClear: true,
      },
    },
    {
      component: 'ApiDict',
      fieldName: 'EQ_enabled',
      label: '是否还原',
      componentProps: {
        placeholder: '请选择',
        allowClear: true,
        code: 'yes_no',
        renderType: 'Select',
      },
    },
  ],
};
// 表单参数定义
export const formSchamas: VbenFormProps = {
  wrapperClass: 'grid-cols-12', // 24栅格,
  commonConfig: {
    formItemClass: 'col-span-12',
  },
  schema: [
    {
      fieldName: 'id',
      label: 'ID',
      component: 'Input',
      ifDetail: false,
      dependencies: {
        show: false,
        triggerFields: ['id'],
      },
    },
    {
      fieldName: 'table_name',
      label: '数据表名称',
      component: 'Input',
      componentProps: {
        placeholder: '',
        allowClear: true,
      },
      formItemClass: 'col-span-12',
      rules: 'required',
      detailSpan: 12,
    },
    {
      fieldName: 'operate_name',
      label: '操作人员',
      component: 'Input',
      componentProps: {
        placeholder: '',
        allowClear: true,
      },
      formItemClass: 'col-span-12',
      rules: 'required',
      detailSpan: 12,
    },
    {
      fieldName: 'ip',
      label: 'IP',
      component: 'Input',
      componentProps: {
        placeholder: '',
        allowClear: true,
      },
      formItemClass: 'col-span-12',
      rules: 'required',
      detailSpan: 12,
    },
    {
      fieldName: 'create_time',
      label: '删除时间',
      component: 'Input',
      componentProps: {
        placeholder: '',
        allowClear: true,
      },
      formItemClass: 'col-span-12',
      rules: 'required',
      detailSpan: 12,
    },
    {
      fieldName: 'enabled',
      label: '是否还原',
      component: 'ApiDict',
      defaultValue: 1,
      componentProps: {
        placeholder: '',
        code: 'yes_no',
        isBtn: true,
        renderType: 'RadioGroup',
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
      rules: 'selectRequired',
    },
    {
      fieldName: 'data',
      label: '数据表名称',
      component: 'Textarea',
      componentProps: {
        placeholder: '',
        allowClear: true,
      },
      formItemClass: 'col-span-12',
      rules: 'required',
      detailSpan: 12,
    }
  ],
};
