import { type VbenFormProps } from '#/adapter';

export const fieldLink={
  root:'存储目录',
  secretId:'secretId',
  accessKey:'accessKey',
  accessKeyId:'accessKeyId',
  secretKey:'secretKey',
  key:'key',
  accessKeySecret:'accessKeySecret',
  secret:'secret',
  bucket:'桶名称',
  dirname:'目录名称',
  domain:'域名地址',
  endpoint:'Endpoint',
  region:'区域',
  acl:'Acl',
  remark:'备注',
}

export const drawerFormSchamas: VbenFormProps = {
  wrapperClass: 'grid-cols-12',
  commonConfig: {
    formItemClass: 'col-span-12',
    labelClass: 'mr-5',
  },
  schema: [
    {
      fieldName: 'mode',
      component: 'Input',
      dependencies: {
        show: () => false,
        triggerFields: [''],
      },
    },
    {
      fieldName: 'root',
      label: fieldLink.root,
      help: '',
      component: 'RadioGroup',
      rules: 'required',
      componentProps: {
        placeholder: '',
        allowClear: true,
        options: [
          {
            label: '不公开（runtime目录）',
            value: 'runtime',
          },
          {
            label: '公开（public目录）',
            value: 'public',
          },
        ],
      },
      dependencies: {
        triggerFields: ['mode'],
        if: (values) => {
          return ['local'].includes(values.mode);
        },
      },
    },
    {
      component: 'Input',
      fieldName: 'secretId',
      label: fieldLink.secretId,
      rules: 'required',
      dependencies: {
        triggerFields: ['mode'],
        if: (values) => {
          return ['cos'].includes(values.mode);
        },
      },
    },
    {
      component: 'Input',
      fieldName: 'accessKey',
      label: fieldLink.accessKey,
      rules: 'required',
      dependencies: {
        triggerFields: ['mode'],
        if: (values) => {
          return ['qiniu'].includes(values.mode);
        },
      },
    },
    {
      component: 'Input',
      fieldName: 'accessKeyId',
      label: fieldLink.accessKeyId,
      rules: 'required',
      dependencies: {
        triggerFields: ['mode'],
        if: (values) => {
          return ['oss'].includes(values.mode);
        },
      },
    },
    {
      component: 'Input',
      fieldName: 'secretKey',
      label: fieldLink.secretKey,
      rules: 'required',
      dependencies: {
        triggerFields: ['mode'],
        if: (values) => {
          return ['cos', 'qiniu'].includes(values.mode);
        },
      },
    },
    {
      component: 'Input',
      fieldName: 'key',
      label: fieldLink.key,
      rules: 'required',
      dependencies: {
        triggerFields: ['mode'],
        if: (values) => {
          return ['s3'].includes(values.mode);
        },
      },
    },
    {
      component: 'Input',
      fieldName: 'accessKeySecret',
      label: fieldLink.accessKeySecret,
      rules: 'required',
      dependencies: {
        triggerFields: ['mode'],
        if: (values) => {
          return ['oss'].includes(values.mode);
        },
      },
    },
    {
      component: 'Input',
      fieldName: 'secret',
      label: fieldLink.secret,
      rules: 'required',
      dependencies: {
        triggerFields: ['mode'],
        if: (values) => {
          return ['s3'].includes(values.mode);
        },
      },
    },
    {
      component: 'Input',
      fieldName: 'bucket',
      label: fieldLink.bucket,
      rules: 'required',
      dependencies: {
        triggerFields: ['mode'],
        if: (values) => {
          return ['oss', 'cos', 'qiniu', 's3'].includes(values.mode);
        },
      },
    },
    {
      component: 'Input',
      fieldName: 'dirname',
      label: fieldLink.dirname,
      rules: 'required',
    },
    {
      component: 'Input',
      fieldName: 'domain',
      label: fieldLink.domain,
      rules: 'required',
      dependencies: {
        triggerFields: ['mode'],
        if: (values) => {
          return ['local', 'oss', 'cos', 'qiniu', 's3'].includes(values.mode);
        },
      },
    },

    {
      component: 'Input',
      fieldName: 'endpoint',
      label: fieldLink.endpoint,
      dependencies: {
        triggerFields: ['mode'],
        if: (values) => {
          return ['oss', 'qiniu', 's3'].includes(values.mode);
        },
      },
    },

    {
      component: 'Input',
      fieldName: 'region',
      label: fieldLink.region,
      dependencies: {
        triggerFields: ['mode'],
        if: (values) => {
          return ['cos', 's3'].includes(values.mode);
        },
      },
    },
    {
      component: 'Input',
      fieldName: 'acl',
      label: fieldLink.acl,
      dependencies: {
        triggerFields: ['mode'],
        if: (values) => {
          return ['s3'].includes(values.mode);
        },
      },
    },
    {
      component: 'Textarea',
      fieldName: 'remark',
      formItemClass: 'items-baseline',
      label: fieldLink.remark,
    },
  ],
};



/**
 * 构建配置数据
 * @param data
 * @param skipFields
 * @param extraFields
 */
export const transformData = function (
  data: { [s: string]: unknown } | ArrayLike<unknown>,
  skipFields: string[] = [],
  allowedKeys: string[] = [], // 添加允许的键集合
  extraFields: Record<string, string> = {},
  filedlink: Record<string, string> = {},
) {



  return Object.entries(data)
    .filter(([key]) =>
      !skipFields.includes(key) && (allowedKeys.length === 0 || allowedKeys.includes(key)) // 过滤跳过字段和不在允许集合中的字段
    )
    .map(([key, value]) => {
      const transformedObject = {
        code: key,
        content: value !== undefined ? value : "",
        name: filedlink[key],
      };

      Object.assign(transformedObject, extraFields);
      return transformedObject;
    });
}

/**
 * 返回模板字段
 * @param groupCode
 * @returns
 */
export const templateField = function (groupCode: string) {
  const data = {
    local: ['root', 'dirname', 'domain','remark'],
    oss: ['accessKeyId','accessKeySecret','bucket','domain','endpoint','dirname','remark'],
    cos: ['secretId','secretKey','bucket','domain','region','dirname','remark'],
    qiniu: ['accessKey','secretKey','bucket','domain','region','dirname','remark'],
    s3: ['key','secret','bucket','dirname','domain','region','version','endpoint','acl','remark'],
  }
  //@ts-ignore
  return data[groupCode] || [];
}

export const convertStringNumbers = (obj: Record<string, any>): Record<string, any> => {
  Object.keys(obj).forEach(key => {
      const value = obj[key];
      if (typeof value === 'string' && value.trim() !== "" && !isNaN(Number(value))) {
          obj[key] = Number(value);
      }
  });
  return obj;
};

