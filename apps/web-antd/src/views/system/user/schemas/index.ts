import type { VbenFormProps, VxeGridProps } from '#/adapter';

import { z } from '#/adapter';
import type PaginationEllipsis from '../../../../../../../packages/@core/ui-kit/shadcn-ui/src/ui/pagination/PaginationEllipsis.vue';
// 表格参数定义
export const gridSchemas: VxeGridProps<any> = {
  toolbarConfig: {
    refresh: true, // 刷新
    print: false, // 打印
    export: false, // 导出
    custom: true, // 自定义列
    zoom: true, // 最大化最小化
  },
  printConfig: {},
  columnConfig: {
    resizable: true,//开启拖拽行宽模式
  },
  columns: [
    { type: 'checkbox', width: 60 },
    { field: 'user_name', title: '用户名' },
    // {
    //   field: 'role_ids',
    //   title: '所属角色',
    //   cellRender: {
    //     name: 'ApiSelect',
    //     props: {
    //       api: '/system/role/select',
    //     },
    //   },
    // },
    { field: 'real_name', title: '姓名' },
    { field: 'nick_name', title: '昵称' },
    {
      field: 'sex',
      title: '性别',
      cellRender: {
        name: 'ApiDict',
        props: {
          code: 'sex',
        },
      },
    },
    {
      field: 'is_locked',
      title: '是否锁定',
      cellRender: {
        name: 'ApiDict',
        props: {
          code: 'yes_no',
        },
      },
    },
    { field: 'mobile_phone', title: '手机号码' },
    { field: 'tel', title: '联系电话' },
    {
      width: 160,
      title: '操作',
      align: 'center',
      slots: { default: 'ACTION' },
      fixed: 'right',
    },
  ],
};


// 搜索表单参数定义
export const searchFormSchemas: VbenFormProps = {
  schema: [
    {
      component: 'Input',
      fieldName: 'LIKE_user_name',
      label: '用户名',
      componentProps: {
        placeholder: '请输入用户名',
        allowClear: true,
      },
    },
    {
      component: 'Input',
      fieldName: 'LIKE_real_name',
      label: '姓名',
      componentProps: {
        placeholder: '请输入姓名',
        allowClear: true,
      },
    },
    {
      component: 'Input',
      fieldName: 'LIKE_mobile_phone',
      label: '手机号码',
      componentProps: {
        placeholder: '请输入手机号码',
        allowClear: true,
      },
    },
  ],
};

// 表单参数定义
export const formSchamas: VbenFormProps = {
  wrapperClass: 'grid-cols-12', // 24栅格,
  commonConfig: {
    formItemClass: 'col-span-12',
  },
  schema: [
    {
      fieldName: 'id',
      label: '用户ID',
      component: 'Input',
      ifDetail: false,
      dependencies: {
        show: false,
        triggerFields: ['id'],
      },
    },
    {
      fieldName: 'baseinfo',
      component: 'Divider',
      label: '基础信息',
      formItemClass: 'col-span-12',
      hideLabel: true,
      componentProps:{
        orientation:'left',
        orientationMargin:10,
      },
      renderComponentContent: () => {
        return {
          default: () => {
            return '基础信息';
          },
        };
      },
    },
    {
      fieldName: 'user_name',
      label: '用户名',
      component: 'Input',
      componentProps: {
        placeholder: '请输入用户名',
        allowClear: true,
      },
      formItemClass: 'col-span-6',
      rules: 'required',
      detailSpan: 6,
    },
    {
      fieldName: 'real_name',
      label: '姓名',
      component: 'Input',
      componentProps: {
        placeholder: '请输入姓名',
        allowClear: true,
      },
      formItemClass: 'col-span-6',
      rules: 'required',
      detailSpan: 6,
    },
    {
      fieldName: 'password',
      label: '密码',
      component: 'InputPassword',
      help: '5-18位数字、字母、特殊字符组成。',
      componentProps: {
        placeholder: '请输入密码',
        allowClear: true,
      },
      formItemClass: 'col-span-6',
      rules: z
        .string()
        .regex(/[\w!@#$%^&*]{5,18}/, '密码由5-18位数字、字母、特殊字符组成。'),
      dependencies: {
        if({ id }) {
          return !id;
        },
        triggerFields: ['id'],
      },
      ifDetail: false,
    },
    {
      fieldName: 'confirm_password',
      label: '确认密码',
      component: 'InputPassword',
      componentProps: {
        placeholder: '请输入确认密码',
        allowClear: true,
      },
      formItemClass: 'col-span-6',
      rules: z
        .string()
        .regex(/[\w!@#$%^&*]{5,18}/, '密码由5-18位数字、字母、特殊字符组成。'),
      dependencies: {
        if({ id }) {
          return !id;
        },
        triggerFields: ['id'],
      },
      ifDetail: false,
    },
    {
      fieldName: 'nick_name',
      label: '昵称',
      component: 'Input',
      componentProps: {
        placeholder: '请输入昵称',
        allowClear: true,
      },
      formItemClass: 'col-span-6',
      detailSpan: 6,
    },
    {
      fieldName: 'sex',
      label: '性别',
      component: 'ApiDict',
      defaultValue: 1,
      componentProps: {
        placeholder: '请选择性别',
        code: 'sex',
        renderType: 'RadioGroup',
        isBtn: true,
      },
      formItemClass: 'col-span-6',
      detailSpan: 6,
    },
    {
      fieldName: 'email',
      label: '邮箱',
      component: 'Input',
      componentProps: {
        placeholder: '请输入邮箱',
        allowClear: true,
      },
      formItemClass: 'col-span-6',
      detailSpan: 6,
    },
    {
      fieldName: 'mobile_phone',
      label: '手机号码',
      component: 'Input',
      componentProps: {
        placeholder: '请输入手机号码',
        allowClear: true,
      },
      formItemClass: 'col-span-6',
      rules: 'required',
      detailSpan: 6,
    },
    {
      fieldName: 'otherinfo',
      component: 'Divider',
      label: '其他信息',
      formItemClass: 'col-span-12',
      hideLabel: true,
      componentProps:{
        orientation:'left',
        orientationMargin:10,
      },
      renderComponentContent: () => {
        return {
          default: () => {
            return '其他信息';
          },
        };
      },
    },
    {
      fieldName: 'dept_id',
      label: '所属部门',
      component: 'ApiTreeSelect',
      componentProps: {
        placeholder: '请选择所属部门',
        allowClear: true,
        api: '/system/dept',
        labelField: 'name',
        valueField: 'id',
        params: {
          format:'tree',
          enabled: 1,
        },
      },
      formItemClass: 'col-span-6',
      rules: 'selectRequired',
      detailSpan: 6,
    },
    {
      fieldName: 'post_id_list',
      label: '职位',
      component: 'ApiSelect',
      componentProps: {
        placeholder: '请选择所属岗位',
        allowClear: true,
        api: '/system/post',
        labelField: 'label',
        valueField: 'value',
        params: {
          format:'select',
          enabled: 1,
          page: 1,
          limit: 1000,
        },
      },
      formItemClass: 'col-span-6',
      // rules: 'selectRequired',
      detailSpan: 6,
    },
    {
      fieldName: 'role_id_list',
      label: '角色',
      component: 'ApiSelect',
      componentProps: {
        placeholder: '请选择角色',
        allowClear: true,
        api: '/system/role',
        mode: 'multiple',
        params: {
          format:'select',
          enabled: 1,
          page: 1,
          limit: 1000,
        },
      },
      formItemClass: 'col-span-12',
    },
    {
      fieldName: 'tel',
      label: '联系电话',
      component: 'Input',
      componentProps: {
        placeholder: '请输入联系电话',
        allowClear: true,
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
    },
    // {
    //   fieldName: 'avatar',
    //   label: '头像',
    //   component: 'Upload',
    //   componentProps: {
    //     placeholder: '请选择头像',
    //   },
    //   formItemClass: 'col-span-12',
    // },
    {
      fieldName: 'remark',
      label: '备注',
      component: 'Textarea',
      componentProps: {
        placeholder: '请输入备注',
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
    },
  ],
};
