import type { VbenFormProps, VxeGridProps } from '#/adapter';
// 表格参数定义
export const gridSchemas: VxeGridProps<any> = {
  toolbarConfig: {
    refresh: true, // 刷新
    print: false, // 打印
    export: false, // 导出
    // custom: true, // 自定义列
    zoom: true, // 最大化最小化
  },
  columnConfig: {
    resizable: true,//开启拖拽行宽模式
  },
  columns: [
    { type: 'checkbox', width: 60 },
    { field: 'filename', title: '名字',align:'left',width:200},
    { field: 'original_filename', title: '文件' },
    { field: 'size_info', title: '文件大小' },
    { field: 'ext', title: '扩展名' },
    { field: 'platform', title: '存储平台' },
    { field: 'create_date', title: '创建时间' },
    { field: 'created_name', title: '上传人' },
    {
      width: 170,
      title: '操作',
      align: 'center',
      slots: { default: 'ACTION' },
      fixed: 'right',
    },
  ],
};


// 搜索表单参数定义
export const searchFormSchemas: VbenFormProps = {
  schema: [
    {
      component: 'Input',
      fieldName: 'LIKE_filename',
      label: '文件名',
      componentProps: {
        placeholder: '文件名称',
        allowClear: true,
      },
    },
    {
      component: 'Input',
      fieldName: 'LIKE_original_filename',
      label: '原名称',
      componentProps: {
        placeholder: '请输入唯一编码',
        allowClear: true,
      },
    },
    {
      component: 'Input',
      fieldName: 'LIKE_ext',
      label: '扩展名',
      componentProps: {
        placeholder: '请输入扩展名称',
        allowClear: true,
      },
    },
    {
      component: 'Input',
      fieldName: 'IN_platform',
      label: '平台',
      componentProps: {
        placeholder: '请输选择平台',
        allowClear: true,
      },
    }
  ],
};


// 表单参数定义
export const formSchamas: VbenFormProps = {
  wrapperClass: 'grid-cols-12', // 24栅格,
  commonConfig: {
    formItemClass: 'col-span-12',
  },
  schema: [
    {
      fieldName: 'id',
      label: '岗位ID',
      component: 'Input',
      ifDetail: false,
      dependencies: {
        show: false,
        triggerFields: ['id'],
      },
    },
    {
      fieldName: 'name',
      label: '职位名称',
      component: 'Input',
      componentProps: {
        placeholder: '请输入岗位名称',
        allowClear: true,
      },
      formItemClass: 'col-span-12',
      rules: 'required',
      detailSpan: 12,
    },
    {
      fieldName: 'code',
      label: '职位标识',
      component: 'Input',
      componentProps: {
        placeholder: '请输入唯一编码',
        allowClear: true,
      },
      formItemClass: 'col-span-12',
      rules: 'required',
      detailSpan: 12,
    },
    {
      fieldName: 'enabled',
      label: '是否启用',
      component: 'ApiDict',
      defaultValue: 1,
      componentProps: {
        placeholder: '请选择是否启用',
        code: 'yes_no',
        isBtn: true,
        renderType: 'RadioGroup',
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
      rules: 'selectRequired',
    },
    {
      fieldName: 'sort',
      label: '排序',
      component: 'InputNumber',
      componentProps: {
        placeholder: '请输入排序',
        allowClear: true,
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
    },
    {
      fieldName: 'remark',
      label: '备注',
      component: 'Textarea',
      componentProps: {
        placeholder: '请输入备注',
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
    },
  ],
};
