import type { VbenFormProps, VxeGridProps } from '#/adapter';

// 表格参数定义
export const gridSchemas: VxeGridProps<any> = {
  toolbarConfig: {
    refresh: true, // 刷新
    print: false, // 打印
    export: false, // 导出
    // custom: true, // 自定义列
    zoom: true, // 最大化最小化
  },
  columnConfig: {
    resizable: true,//开启拖拽行宽模式
  },
  columns: [
    { type: 'checkbox', width: 60 },
    { field: 'name', title: '模块',align:'left' },
    { field: 'app', title: 'App' },
    { field: 'ip', title: 'ip' },
    { field: 'ip_location', title: '归属地' },
    { field: 'os', title: '操作系统' },
    { field: 'browser', title: '浏览器' },
    { field: 'url', title: '请求地址',align:'left',minWidth:170},
    { field: 'browser', title: '请求参数',align:'left'},
    { field: 'create_time', title: '日期',width:170},
    {
      width: 170,
      title: '操作',
      align: 'center',
      slots: { default: 'ACTION' },
      fixed: 'right',
    },
  ],
};
// 搜索表单参数定义
export const searchFormSchemas: VbenFormProps = {
  schema: [
    {
      component: 'Input',
      fieldName: 'LIKE_user_name',
      label: '用户名',
      componentProps: {
        placeholder: '请输入用户名称',
        allowClear: true,
      },
    }
  ],
};


// 表单参数定义
export const formSchamas: VbenFormProps = {
  wrapperClass: 'grid-cols-12', // 24栅格,
  commonConfig: {
    formItemClass: 'col-span-12',
  },
  schema: [
    {
      fieldName: 'id',
      label: '日志ID',
      component: 'Textarea',
      ifDetail: false,
      dependencies: {
        show: false,
        triggerFields: ['id'],
      },
    },
    {
      fieldName: 'name',
      label: '内容',
      component: 'Textarea',
      ifDetail: false,
      componentProps: {
        placeholder: '请输入角色名称',
        allowClear: true,
      },
      formItemClass: 'col-span-12',
      rules: 'required',
      detailSpan: 6,
    },
    {
      fieldName: 'name',
      label: '操作模块',
      component: 'Textarea',
      componentProps: {
        placeholder: '请输入唯一编码',
        allowClear: true,
      },
      formItemClass: 'col-span-12',
      rules: 'required',
      detailSpan: 6,
    },
    {
      fieldName: 'url',
      label: '请求地址',
      component: 'Textarea',
      formItemClass: 'col-span-12',
      detailSpan: 6,
    },
    {
      fieldName: 'class_name',
      label: '类名称',
      component: 'Textarea',
      componentProps: {
        placeholder: '请输入备注',
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
    },
    {
      fieldName: 'method',
      label: '请求方式',
      component: 'Textarea',
      componentProps: {
        placeholder: '请输入备注',
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
    },
    {
      fieldName: 'action',
      label: '操作方法',
      component: 'Textarea',
      componentProps: {
        placeholder: '请输入备注',
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
    },
    {
      fieldName: 'create_time',
      label: '操作时间',
      component: 'Textarea',
      componentProps: {
        placeholder: '请输入备注',
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
    },
    {
      fieldName: 'param',
      label: '请求参数',
      component: 'Textarea',
      componentProps: {
        placeholder: '请输入备注',
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
    },
    {
      fieldName: 'result',
      label: '返回参数',
      component: 'Textarea',
      componentProps: {
        placeholder: '请输入备注',
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
    },

  ],
};

