import type { VbenFormProps, VxeGridProps } from '#/adapter';

// 表格参数定义
export const gridSchemas: VxeGridProps<any> = {
  toolbarConfig: {
    refresh: true, // 刷新
    print: false, // 打印
    export: false, // 导出
    // custom: true, // 自定义列
    zoom: true, // 最大化最小化
  },
  columnConfig: {
    resizable: true,//开启拖拽行宽模式
  },
  columns: [
    { type: 'checkbox', width: 60 },
    { field: 'message', title: '内容',align:'left' },
    { field: 'user_name', title: '用户名' },
    { field: 'os', title: '操作系统' },
    { field: 'browser', title: '浏览器' },
    { field: 'ip', title: 'ip' },
    { field: 'ip_location', title: '归属地' },
    { field: 'create_time', title: '日期' },
  ],
};
// 搜索表单参数定义
export const searchFormSchemas: VbenFormProps = {
  schema: [
    {
      component: 'Input',
      fieldName: 'LIKE_user_name',
      label: '用户名',
      componentProps: {
        placeholder: '请输入用户名称',
        allowClear: true,
      },
    }
  ],
};

