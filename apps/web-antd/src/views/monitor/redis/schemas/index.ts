
// 参数定义
export const cloums = [
  {
    field: "redis_version",
    label: "Redis版本",
    span: 6,
  },
  {
    field: "redis_mode",
    label: "Redis模式",
    span: 6,
  },
  {
    field: "os",
    label: "操作系统",
    span: 6,
  },
  {
    field: "arch_bits",
    label: "架构位数",
    span: 6,
  },
  {
    field: "multiplexing_api",
    label: "多路复用 API",
    span: 6,
  },
  {
    field: "process_id",
    label: "进程ID",
    span: 6,
  },
    {
      field: "run_id",
      label: "运行ID",
      span: 6,
    },
  {
    field: "tcp_port",
    label: "TCP 端口",
    span: 6,
  },
  {
    field: "uptime_in_days",
    label: "运行时间(天)",
    span: 6,
  },
  {
    field: "role",
    label: "角色",
    span: 6,
  },
  {
    field: "connected_clients",
    label: "客户端数量",
    span: 6,
  },
  {
    field: "arch_bits",
    label: "架构位数",
    span: 6,
  },

  {
    field: "used_memory_human",
    label: "以使用内存",
    span: 6,
  },
  {
    field: "used_memory_peak_human",
    label: "峰值已使用内存",
    span: 6,
  },
  {
    field: "mem_allocator",
    label: "内存分配器",
    span: 6,
  },
  {
    field: "total_connections_received",
    label: "接收到的总连接数",
    span: 6,
  },
  {
    field: "total_commands_processed",
    label: "处理的总命令数",
    span: 6,
  },
  {
    field: "instantaneous_ops_per_sec",
    label: "每秒瞬时操作数",
    span: 6,
  },
  {
    field: "total_net_input_bytes",
    label: "总网络输入字节数",
    span: 6,
  },

  {
    field: "instantaneous_input_kbps",
    label: "瞬时输入速率（KB/s）",
    span: 6,
  },
  {
    field: "instantaneous_output_kbps",
    label: "瞬时输出速率（KB/s）",
    span: 6,
  },
  {
    field: "rejected_connections",
    label: "被拒绝的连接数",
    span: 6,
  },
  {
    field: "keyspace_hits",
    label: "键空间命中次数",
    span: 6,
  },
  {
    field: "keyspace_misses",
    label: "键空间未命中次数",
    span: 6,
  },
  {
    field: "config_file",
    label: "配置文件路径",
    span: 8,
  }
];
