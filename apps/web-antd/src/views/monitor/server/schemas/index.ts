import type { VbenFormProps, VxeGridProps } from '#/adapter';


// CPU信息定义
export const gridSchemas: VxeGridProps<any> = {
  toolbarConfig: {
    refresh: false, // 刷新
    print: false, // 打印
    export: false, // 导出
    zoom: false, // 最大化最小化
    enabled:false,

  },
  columnConfig: {
    resizable: false,//开启拖拽行宽模式
  },
  columns: [
    {
      field: 'key',
      title: '属性',
      align:'left',
      width:170,
      cellRender: {
        name: 'ApiDict',
        props: {
          code: 'monitor_server_cpu',
        },
      },
    },
    { field: 'value', title: '值',align:'left' },
  ],
};


/**
 * 内存信息
 */
export const gridMemorySchemas: VxeGridProps<any> = {
  toolbarConfig: {
    refresh: false, // 刷新
    print: false, // 打印
    export: false, // 导出
    zoom: false, // 最大化最小化
    enabled:false,
  },
  columnConfig: {
    resizable: false,//开启拖拽行宽模式
  },
  columns: [
    {
      field: 'key',
      title: '属性',
      align:'left',
      width:170,
      cellRender: {
        name: 'ApiDict',
        props: {
          code: 'monitor_server_memory',
        },
      },
    },
    { field: 'value', title: '值',align:'left' },
  ],
};


export const gridPhpSchemas: VxeGridProps<any> = {
  toolbarConfig: {
    refresh: false, // 刷新
    print: false, // 打印
    export: false, // 导出
    zoom: false, // 最大化最小化
    enabled:false,
  },
  columnConfig: {
    resizable: false,//开启拖拽行宽模式
  },
  columns: [
    {
      field: 'key',
      title: '属性',
      align:'left',
      width:170,
      cellRender: {
        name: 'ApiDict',
        props: {
          code: 'monitor_server_memory',
        },
      },
    },
    { field: 'value', title: '值',align:'left' },
  ],
};


export const gridDiskSchemas: VxeGridProps<any> = {
  toolbarConfig: {
    refresh: false, // 刷新
    print: false, // 打印
    export: false, // 导出
    zoom: false, // 最大化最小化
    enabled:false,
  },
  columnConfig: {
    resizable: false,//开启拖拽行宽模式
  },
  columns: [
    { field: 'filesystem', title: '文件系统',align:'left' },
    { field: 'size', title: '总大小',align:'left' },
    { field: 'available', title: '可用空间',align:'left' },
    { field: 'used', title: '已用空间',align:'left' },
    { field: 'use_percentage', title: '使用百分比',align:'left' },
    { field: 'mounted_on', title: '挂载点',align:'left' },
  ],
};
