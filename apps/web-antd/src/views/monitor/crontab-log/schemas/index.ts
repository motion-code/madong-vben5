import type { VbenFormProps, VxeGridProps } from '#/adapter';
// 表格参数定义
export const gridSchemas: VxeGridProps<any> = {
  toolbarConfig: {
    refresh: true, // 刷新
    print: false, // 打印
    export: false, // 导出
    // custom: true, // 自定义列
    zoom: true, // 最大化最小化
  },
  columns: [
    { type: 'checkbox', width: 60 },
    { field: 'target', title: '调用目标',width:170 },
    { field: 'running_time', title: '执行耗时' },
    { field: 'return_code', title: '状态',slots: { default: 'returnCode' } },
    { field: 'log', title: '执行日志' },
    { field: 'create_time', title: '执行时间' },
    {
      width: 160,
      title: '操作',
      align: 'center',
      slots: { default: 'ACTION' },
      fixed: 'right',
    },
  ],
};
// 搜索表单参数定义
export const searchFormSchemas: VbenFormProps = {
  schema: [
    {
      component: 'Input',
      fieldName: 'LIKE_target',
      label: '调用目标',
      componentProps: {
        placeholder: '请输入调用目标',
        allowClear: true,
      },
    },
    {
      component: 'ApiDict',
      fieldName: 'EQ_enabled',
      label: '是否成功',
      componentProps: {
        placeholder: '请选择',
        allowClear: true,
        code: 'yes_no',
        renderType: 'Select',
      },
    },
  ],
};
