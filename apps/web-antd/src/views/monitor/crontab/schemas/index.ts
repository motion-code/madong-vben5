import type { VbenFormProps, VxeGridProps } from '#/adapter';

import { h } from 'vue';

// 表格参数定义
export const gridSchemas: VxeGridProps<any> = {
  toolbarConfig: {
    refresh: true, // 刷新
    print: false, // 打印
    export: false, // 导出
    // custom: true, // 自定义列
    zoom: true, // 最大化最小化
  },
  columnConfig: {
    resizable: true,//开启拖拽行宽模式
  },
  columns: [
    { type: 'checkbox', width: 60 },
    { field: 'title', title: '任务名称', align: 'left' },
    {
      field: 'rule',
      title: '执行表达式',
      align:'left'
    },
    {
      field: 'enabled',
      title: '是否启用',
      cellRender: {
        name: 'ApiDict',
        props: {
          code: 'yes_no',
        },
      },
    },
    {
      field: 'singleton',
      title: '模式',
      cellRender: {
        name: 'ApiDict',
        props: {
          code: 'monitor_crontab_mode',
        },
      },
    },
    { field: 'rule_name',align:'left', title: '执行周期' },
    { field: 'target', title: '调用目标', align: 'left', minWidth: 170 },
    { field: 'running_times', title: '运行次数', align: 'left' },
    { field: 'last_running_time', title: '上次运行时间', width: 170 },
    {
      width: 200,
      title: '操作',
      align: 'center',
      slots: { default: 'ACTION' },
      fixed: 'right',
    },
  ],
};

// 搜索表单参数定义
export const searchFormSchemas: VbenFormProps = {
  schema: [
    {
      component: 'Input',
      fieldName: 'LIKE_title',
      label: '任务名称',
      componentProps: {
        placeholder: '请输入任务名称',
        allowClear: true,
      },
    },
    {
      component: 'ApiDict',
      fieldName: 'IN_singleton',
      label: '模式',
      componentProps: {
        placeholder: '请选择',
        allowClear: true,
        code: 'monitor_crontab_mode',
        renderType: 'Select',
      },
    },
    {
      component: 'ApiDict',
      fieldName: 'EQ_enabled',
      label: '是否启用',
      componentProps: {
        placeholder: '请选择',
        allowClear: true,
        code: 'yes_no',
        renderType: 'Select',
      },
    },
  ],
};


// 表单参数定义
export const formSchamas: VbenFormProps = {
  wrapperClass: 'grid-cols-12', // 24栅格,
  commonConfig: {
    formItemClass: 'col-span-12',
  },
  schema: [
    {
      fieldName: 'id',
      label: '任务ID',
      component: 'Input',
      ifDetail: false,
      dependencies: {
        show: false,
        triggerFields: ['id'],
      },
    },
    {
      fieldName: 'title',
      label: '任务名称',
      component: 'Input',
      componentProps: {
        placeholder: '请输入定时任务名称',
        allowClear: true,
      },
      formItemClass: 'col-span-12',
      rules: 'required',
      detailSpan: 12,
    },
    {
      fieldName: 'type',
      label: '任务类型',
      component: 'ApiDict',
      defaultValue: 1,
      componentProps: {
        placeholder: '请选择是否启用',
        code: 'monitor_crontab_type',
        isBtn: true,
        renderType: 'Select',
      },
      formItemClass: 'col-span-12',
      detailSpan: 6,
      rules: 'selectRequired',
    },
    {
      fieldName: 'rule',
      label: '表达式',
      component: 'Input',
      componentProps: {
        placeholder: '请输入定时任务名称',
        allowClear: true,
      },
      dependencies: {
        triggerFields: ['id'],
        show: (values) => {
          return values.id!==undefined&&values.id!==''&&values.id!==null;
        },
        disabled: (values) => {
          return values.id!==undefined&&values.id!==''&&values.id!==null;
        },
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
    },
    {
      fieldName: 'task_cycle',
      label: '执行周期',
      component: 'ApiDict',
      defaultValue: '',
      componentProps: {
        placeholder: '请选择',
        code: 'monitor_crontab_cycle',
        renderType: 'Select',
      },
      formItemClass: 'col-span-4',
      rules: 'selectRequired',
    },
    {
      fieldName: 'month',
      label: '',
      labelWidth:0,
      suffix: () => '月',
      component: 'InputNumber',
      formItemClass: 'col-span-2',
      dependencies: {
        triggerFields: ['task_cycle'],
        show: (values) => {
          //年份月份的时候显示月份输入
          return [8].includes(values.task_cycle);
        },
      },
      componentProps:{
        min:1,
        max:12
      }
    },
    {
      fieldName: 'day',
      label: '',
      labelWidth:0,
      suffix: () => '日',
      component: 'InputNumber',
      formItemClass: 'col-span-2',
      dependencies: {
        triggerFields: ['task_cycle'],
        show: (values) => {
          //年份月份的时候显示
          return [7,8].includes(values.task_cycle);
        },
      },
      componentProps:{
        min:1,
        max:31
      }
    },
    {
      fieldName: 'week',
      label: '',
      labelWidth:1,
      component: 'Select',
      dependencies: {
        triggerFields: ['task_cycle'],
        show: (values) => {
          //每星期的时候显示周选项
          return [6].includes(values.task_cycle);
        },
      },
      componentProps: {
        allowClear: true,
        filterOption: true,
        options: [
          {
            label: '周一',
            value: 1,
          },
          {
            label: '周二',
            value: 2,
          },
          {
            label: '周三',
            value: 3,
          },
          {
            label: '周四',
            value: 4,
          },
          {
            label: '周五',
            value: 5,
          },
          {
            label: '周六',
            value: 6,
          },
          {
            label: '周日',
            value: 0,
          },
        ],
        placeholder: '',
        showSearch: true,
      },
      formItemClass: 'col-span-2',
    },
    {
      fieldName: 'hour',
      label: '',
      labelWidth:0,
      suffix: () => '时',
      component: 'InputNumber',
      formItemClass: 'col-span-2',
      dependencies: {
        triggerFields: ['task_cycle'],
        show: (values) => {
          //每星期的时候显示周选项
          return [1,3,6,7,8].includes(values.task_cycle);
        },
      },
      componentProps:{
        min:1,
        max:23
      }
    },
    {
      fieldName: 'minute',
      label: '',
      labelWidth:0,
      suffix: () => '分',
      component: 'InputNumber',
      formItemClass: 'col-span-2',
      dependencies: {
        triggerFields: ['task_cycle'],
        show: (values) => {
          return [1,2,3,4,6,7,8].includes(values.task_cycle);
        },
      },
      componentProps:{
        min:0,
        max:59
      }
    },
    {
      fieldName: 'second',
      label: '',
      labelWidth:0,
      suffix: () => '秒',
      component: 'InputNumber',
      formItemClass: 'col-span-2',
      dependencies: {
        triggerFields: ['task_cycle'],
        show: (values) => {
          return [5].includes(values.task_cycle);
        },
      },
      componentProps:{
        min:0,
        max:59
      }
    },
    {
      fieldName: 'target',
      label: '调用目标',
      component: 'Textarea',
      componentProps: {
        placeholder: '请输入调用目标',
        allowClear: true,
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
      rules: 'required',
    },
    {
      fieldName: 'singleton',
      label: '执行周期',
      component: 'ApiDict',
      defaultValue: 1,
      componentProps: {
        placeholder: '请选择',
        code: 'monitor_crontab_mode',
        renderType: 'RadioGroup',
      },
      formItemClass: 'col-span-12',
      rules: 'selectRequired',
    },
    {
      fieldName: 'enabled',
      label: '启用状态',
      component: 'ApiDict',
      defaultValue: 1,
      componentProps: {
        placeholder: '请选择',
        code: 'yes_no',
        isBtn: true,
        renderType: 'RadioGroup',
      },
      formItemClass: 'col-span-12',
      rules: 'selectRequired',
    },
    {
      fieldName: 'remark',
      label: '备注',
      component: 'Textarea',
      componentProps: {
        placeholder: '请输入备注',
      },
      formItemClass: 'col-span-12',
      detailSpan: 12,
    },
  ],
};

