import { requestClient } from '#/api/request';


const baseUrl = '/system/logs';

export const loginLogs = (params?: Record<string, any>) => {
  return requestClient.get(`${baseUrl}/login`, { params });
}

export const operateLogs = (params?: Record<string, any>) => {
  return requestClient.get(`${baseUrl}/operate`, { params });
}

export const getLoginLog = (id: any) => {
  return requestClient.get(`${baseUrl}/login/${id}`);
}


export const getOperateLog = (id: any) => {
  return requestClient.get(`${baseUrl}/operate/${id}`);
}
