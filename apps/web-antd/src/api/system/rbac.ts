import { requestClient } from '#/api/request';




/**
 * 根据角色ID获取菜单ID集合
 * @param params
 */
export async function roleMenuIds(params: any) {
  return requestClient.get<any>('/system/auth/role-menu-ids', { params });
}

/**
 * 通过角色ID获取用户列表
 * @param params
 */
export async function userListByRoleId(params: any) {
  return requestClient.get<any>('/system/auth/user-list-by-role-id', { params });
}

/**
 * 获取用户列表-排除指定角色
 * @param params
 */
export async function userListExcludeRoleId(params: any) {
  return requestClient.get<any>('/system/auth/user-list-exclude-role-id', { params });
}

/**
 * 删除用户角色关系
 * @param data
 */
export async function removeUserRole(data: any) {
  return requestClient.post<any>('/system/auth/remove-user-role', data);
}
/**
 * 保存角色菜单关系
 * @param data
 */
export async function saveRoleMenu(data: any) {
  return requestClient.post<any>('/system/auth/save-role-menu', data);
}
/**
 * 添加用户角色关系
 * @param data
 */
export async function saveUserRole(data: any) {
  return requestClient.post<any>('/system/auth/save-user-role', data);
}
