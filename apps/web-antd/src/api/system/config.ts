import { requestClient } from '#/api/request';


const baseUrl = '/system/config';

export const info = (params?: Record<string, any>) => {
  return requestClient.get(`${baseUrl}/info`, { params });
}

/**
 * 保存配置
 * @param data
 * @returns
 */
export const save = (data: Record<string, any>) => {
  return requestClient.post(baseUrl, data);
}

