import { requestClient } from '#/api/request';



const baseUrl = '/system/dict';

export const list = (params?: Record<string, any>) => {
  return requestClient.get(baseUrl, { params });
}

export const show = (id: string | number) => {
  return requestClient.get(`${baseUrl}/${id}`);
}

export const save = (data: Record<string, any>) => {
  return requestClient.post(baseUrl, data);
}

export const update = (id: string | number, data: Record<string, any>) => {
  return requestClient.put(`${baseUrl}/${id}`, data);
}

export const destroy = (id: string | number, data?: Record<string, any>) => {
  return requestClient.delete(`${baseUrl}/${id}`, { data });
}

/**
 * 枚举字典
 * @param data 参数
 */
export async function enumDictList(data: any) {
  return requestClient.post('/system/dict/enum-dict-list', data);
}
/**
 * 自定义字典
 * @param data 参数
 */
export async function customDictList(data: any) {
  return requestClient.post('/system/dict/custom-dict-list', data);
}
