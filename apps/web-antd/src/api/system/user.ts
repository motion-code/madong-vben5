import { requestClient } from '#/api/request';

const baseUrl = '/system/user';

export const list = (params?: Record<string, any>) => {
  return requestClient.get(baseUrl, { params });
}

export const show = (id: string | number) => {
  return requestClient.get(`${baseUrl}/${id}`);
}

export const save = (data: Record<string, any>) => {
  return requestClient.post(baseUrl, data);
}

export const update = (id: string | number, data: Record<string, any>) => {
  return requestClient.put(`${baseUrl}/${id}`, data);
}

export const remove = (id: string | number, data?: Record<string, any>) => {
  return requestClient.delete(`${baseUrl}/${id}`, { data });
}

/**
 *
 * @param data 重置密码
 * @returns
 */
export const resetPassword = (data: Record<string, any>) => {
  return requestClient.post(baseUrl+'/reset-password', data);
}

/**
 * 扮演用户
 * @param data 参数
 */
export async function playUser(data: any) {
  return requestClient.post(baseUrl+'/play-user', data);
}
/**
 * 退出扮演
 * @param data 参数
 */
export async function unPlayUser(data: any) {
  return requestClient.post(baseUrl+'/unplay-user', data);
}

/**
 * 授权角色
 * @param data 参数
 */
export async function grantRole(data: any) {
  return requestClient.post(baseUrl+'/grant-role', data);
}

/**
 * 锁定用户
 * @param data 参数
 */
export async function locked(data: any) {
  return requestClient.post(baseUrl+'/locked', data);
}

/**
 * 取消锁定用户
 * @param data 参数
 */
export async function unLocked(data: any) {
  return requestClient.post(baseUrl+'/un-locked', data);
}

/**
 * 修改头像-个人
 * @param data 参数
 */
export async function updateAvatar(data: any) {
  return requestClient.post(baseUrl+'/update-avatar', data);
}
/**
 * 更新用户信息-个人
 * @param data 参数
 */
export async function updateInfo(data: any) {
  return requestClient.post(baseUrl+'/update-info', data);
}
/**
 * 更新用户密码-个人
 * @param data 参数
 */
export async function updatePwd(data: any) {
  return requestClient.post(baseUrl+'/update-pwd', data);
}


/**
 * 我的在线设备
 * @param data 参数
 */
export async function onlineDevice(data: any) {
  return requestClient.post(baseUrl+'/online-device', data);
}

/**
 * 踢用户下线
 * @param data 参数
 */
export async function kickoutByTokenValue(data: any) {
  return requestClient.post(baseUrl+'/kickout-by-token-value', data);
}





