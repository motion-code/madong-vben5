import { requestClient } from '#/api/request';

const baseUrl = '/system/recycle-bin';

export const list = (params?: Record<string, any>) => {
  return requestClient.get(baseUrl, { params });
}

export const show = (id: string | number) => {
  return requestClient.get(`${baseUrl}/${id}`);
}


export const recover = (id: string | number, data: Record<string, any>) => {
  return requestClient.put(`${baseUrl}/${id}`, data);
}

export const destroy = (id: string | number, data?: Record<string, any>) => {
  return requestClient.delete(`${baseUrl}/${id}`, { data });
}
