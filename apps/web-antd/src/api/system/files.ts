import { requestClient } from '#/api/request';


const baseUrl = '/system/files';

export const list = (params?: Record<string, any>) => {
  return requestClient.get(baseUrl, { params });
}

export const show = (id: string | number) => {
  return requestClient.get(`${baseUrl}/${id}`);
}

export const destroy = (id: string | number, data?: Record<string, any>) => {
  return requestClient.delete(`${baseUrl}/${id}`, { data });
}



/**
 * 文件上传
 * @param data
 */
export async function uploadFile(data: any) {
  return requestClient.upload(`${baseUrl}/upload-image`, data);
}



/**
 * 文件下载
 * @param id
 * @returns
 */
export const downloadById = (id: string | number) => {
  return requestClient.get(`${baseUrl}/download-by-id/${id}`, {
     responseType: 'blob',
    isResponse: true
  }as any);
}
