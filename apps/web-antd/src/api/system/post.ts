import { requestClient } from '#/api/request';

const baseUrl = '/system/post';

export const list = (params?: Record<string, any>) => {
  return requestClient.get(baseUrl, { params });
}

export const show = (id: string | number) => {
  return requestClient.get(`${baseUrl}/${id}`);
}

export const save = (data: Record<string, any>) => {
  return requestClient.post(baseUrl, data);
}

export const update = (id: string | number, data: Record<string, any>) => {
  return requestClient.put(`${baseUrl}/${id}`, data);
}

export const destroy = (id: string | number, data?: Record<string, any>) => {
  return requestClient.delete(`${baseUrl}/${id}`, { data });
}
