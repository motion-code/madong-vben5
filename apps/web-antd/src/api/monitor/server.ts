import { requestClient } from '#/api/request';

const baseUrl = '/monitor/server';



export const list = (params?: Record<string, any>) => {
  return requestClient.get(baseUrl, { params });
}

