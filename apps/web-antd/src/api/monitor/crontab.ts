import { requestClient } from '#/api/request';

const baseUrl = '/monitor/crontab';



export const list = (params?: Record<string, any>) => {
  return requestClient.get(baseUrl, { params });
}

export const show = (id: string | number) => {
  return requestClient.get(`${baseUrl}/${id}`);
}

export const save = (data: Record<string, any>) => {
  return requestClient.post(baseUrl, data);
}

export const update = (id: string | number, data: Record<string, any>) => {
  return requestClient.put(`${baseUrl}/${id}`, data);
}

export const destroy = (id: string | number, data?: Record<string, any>) => {
  return requestClient.delete(`${baseUrl}/${id}`, { data });
}

/**
 * 启动
 * @param data
 * @returns
 */
export const start = (data: Record<string, any>) => {
  return requestClient.post(`${baseUrl}/start`, data);
}

/**
 * 恢复
 * @param data
 * @returns
 */
export const resume = (data: Record<string, any>) => {
  return requestClient.post(`${baseUrl}/resume`, data);
}

/**
 * 暂停
 * @param data
 * @returns
 */
export const pause = (data: Record<string, any>) => {
  return requestClient.post(`${baseUrl}/pause`, data);
}

/**
 * 立即执行
 * @param data
 * @returns
 */
export const execute = (data: Record<string, any>) => {
  return requestClient.post(`${baseUrl}/execute`, data);
}
