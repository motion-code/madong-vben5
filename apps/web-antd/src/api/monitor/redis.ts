import { requestClient } from '#/api/request';

const baseUrl = '/monitor/redis';



export const list = (params?: Record<string, any>) => {
  return requestClient.get(baseUrl, { params });
}

