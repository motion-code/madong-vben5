import type { RouteRecordStringComponent } from '@vben/types';

import { $t } from '@vben/locales';

/**
 * 可以扫描staitic目录  将RouteRecordRaw转换RouteRecordStringComponent
 */
const personal: RouteRecordStringComponent[] = [
  {
    component: 'BasicLayout',
    meta: {
      icon: 'ant-design:user-outlined',
      keepAlive: true,
      order: 1000,
      title: '个人中心',
      hideInMenu: true,
      component: 'BasicLayout',
    },
    name: 'personal:manager',
    path: '/system/manager',
    children: [
      {
        meta: {
          icon: 'ant-design:user-switch-outlined',
          title: '个人中心',
          hideInMenu: true,
        },
        name: 'system:personal',
        path: '/system/personal/index',
        component: '/system/personal/index',
      },
    ],
  },
];

/**
 * 后端模式-前端静态路由
 */
export const backendStaticRoutesList: RouteRecordStringComponent[] = [
  {
    component: 'BasicLayout',
    meta: {
      hideChildrenInMenu: true,
      icon: 'lucide:copyright',
      order: 9999,
      title: $t('demos.vben.about'),
    },
    name: 'About',
    path: '/about',
    children: [
      {
        component: '/_core/about/index',
        meta: {
          title: $t('demos.vben.about'),
        },
        name: 'VbenAbout',
        path: '/vben-admin/about',
      },
    ],
  },
  ...personal
];
