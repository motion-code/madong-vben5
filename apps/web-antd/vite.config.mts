import { defineConfig } from '@vben/vite-config';

export default defineConfig(async () => {
  return {
    application: {
      nitroMock: false,//关闭内置Mock
    },
    vite: {
      build: {
        outDir: '../../../server/public', // 指定打包后的输出目录到后端入口如果前后端分离需要变更
      },
      server: {
        proxy: {
          '/api': {
            changeOrigin: true,
            rewrite: (path) => path.replace(/^\/api/, ''),
            // mock代理目标地址
            // target: 'http://43.138.153.216:8899',
            target: 'http://127.0.0.1:8899',
            // target: 'https://apifoxmock.com/m1/5291455-0-default',
            ws: true,
          },
        },
      },
    },
  };
});
