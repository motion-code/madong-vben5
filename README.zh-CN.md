## 简介

madong-vben基于vben5的快速开发框架前端工程

#### 特性

- **最新技术栈**：使用 Vue3/vite 等前端前沿技术开发
- **TypeScript**: 应用程序级 JavaScript 的语言
- **主题**：提供多套主题色彩，可配置自定义主题
- **国际化**：内置完善的国际化方案
- **权限** 内置完善的动态路由权限生成方案

#### 预览

- [antd-vben5](http://antd-vben5.madong.tech/) - 完整版中文站点

测试账号: admin/123456



#### 文档

[文档地址](https://doc.vben.pro/)



#### 代码仓库
- Gitee   仓库地址：https://gitee.com/motion-code/madong-vben5
- Gitcode 仓库地址：https://gitcode.com/motion-code/madong-vben5


#### 后端仓库
- Gitee   仓库地址：https://gitee.com/motion-code/madong
- Gitcode 仓库地址：https://gitcode.com/motion-code/madong


#### 安装使用

- 安装依赖

```bash
cd madong-vben5

corepack enable

pnpm install
```

- 运行

```bash
pnpm dev
```

- 打包

```bash
pnpm build
```

#### 更新日志

[CHANGELOG](https://gitee.com/motion-code/madong-vben5/commits/master)

#### 如何贡献

非常欢迎你的加入！[提一个 Issue](https://gitee.com/motion-code/madong-vben5/issues) 或者提交一个 Pull Request。

**Pull Request:**

1. Fork 代码!
2. 创建自己的分支: `git checkout -b feature/xxxx`
3. 提交你的修改: `git commit -am 'feat(function): add xxxxx'`
4. 推送您的分支: `git push origin feature/xxxx`
5. 提交`pull request`

#### Git 贡献提交规范

- 参考 [vue](https://github.com/vuejs/vue/blob/dev/.github/COMMIT_CONVENTION.md) 规范 ([Angular](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-angular))

  - `feat` 增加新功能
  - `fix` 修复问题/BUG
  - `style` 代码风格相关无影响运行结果的
  - `perf` 优化/性能提升
  - `refactor` 重构
  - `revert` 撤销修改
  - `test` 测试相关
  - `docs` 文档/注释
  - `chore` 依赖更新/脚手架配置修改等
  - `ci` 持续集成
  - `types` 类型定义文件更改
  - `wip` 开发中

#### 浏览器支持

本地开发推荐使用`Chrome 80+` 浏览器

支持现代浏览器, 不支持 IE

| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt=" Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>IE | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt=" Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Edge | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari |
| :-: | :-: | :-: | :-: | :-: |
| not support | last 2 versions | last 2 versions | last 2 versions | last 2 versions |



#### 联系我

如果您对开源技术感兴趣，或者希望与我交流合作，欢迎通过以下方式联系我：

- **频道**：[pd52261144](https://pd.qq.com/s/3edfwx2lm)

- **邮箱**：[405784684@qq.com]

- **社区**：[https://madong.tech](https://madong.tech)


#### 维护者

[Mr.April](https://gitee.com/liu_guan_qing)
