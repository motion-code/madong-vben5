## Introduction

madong-vben is a front-end engineering rapid development framework based on vben5.

#### Features

- **Latest Tech Stack**: Developed using cutting-edge front-end technologies such as Vue3 and Vite.
- **TypeScript**: A language for application-level JavaScript.
- **Themes**: Offers multiple theme colors with customizable options.
- **Internationalization**: Built-in comprehensive internationalization support.
- **Permissions**: Comes with a robust dynamic routing permission generation scheme.

#### Preview

- [antd-vben5](http://antd-vben5.madong.tech/) - Complete Chinese site

Test account: admin/123456

#### Documentation

[Documentation Link](https://doc.vben.pro/)

#### Code Repositories
- Gitee Repository: [https://gitee.com/motion-code/madong-vben5](https://gitee.com/motion-code/madong-vben5)
- Gitcode Repository: [https://gitcode.com/motion-code/madong-vben5](https://gitcode.com/motion-code/madong-vben5)

#### Backend Repository
- Gitee Repository: [https://gitee.com/motion-code/madong](https://gitee.com/motion-code/madong)
- Gitcode Repository: [https://gitcode.com/motion-code/madong](https://gitcode.com/motion-code/madong)

#### Installation and Usage

- Install Dependencies

```bash
cd madong-vben5

corepack enable

pnpm install
```

- Run

```bash
pnpm dev
```

- Build

```bash
pnpm build
```

#### Changelog

[CHANGELOG](https://gitee.com/motion-code/madong-vben5/commits/master)

#### How to Contribute

We warmly welcome your participation! [Open an Issue](https://gitee.com/motion-code/madong-vben5/issues) or submit a Pull Request.

**Pull Request:**

1. Fork the repository!
2. Create your own branch: `git checkout -b feature/xxxx`
3. Commit your changes: `git commit -am 'feat(function): add xxxxx'`
4. Push your branch: `git push origin feature/xxxx`
5. Submit a Pull Request

#### Git Contribution Commit Guidelines

- Refer to the [vue](https://github.com/vuejs/vue/blob/dev/.github/COMMIT_CONVENTION.md) guidelines ([Angular](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-angular))

  - `feat` New feature
  - `fix` Bug fix
  - `style` Code style changes that do not affect the output
  - `perf` Performance improvements
  - `refactor` Code refactoring
  - `revert` Revert changes
  - `test` Related to tests
  - `docs` Documentation/comments
  - `chore` Dependency updates/scripting configuration changes, etc.
  - `ci` Continuous integration
  - `types` Changes to type definition files
  - `wip` Work in progress

#### Browser Support

For local development, it is recommended to use `Chrome 80+`.

Supports modern browsers, does not support IE.

| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt=" Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>IE | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt=" Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Edge | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari |
| :-: | :-: | :-: | :-: | :-: |
| not supported | last 2 versions | last 2 versions | last 2 versions | last 2 versions |

#### Contact Me

If you are interested in open-source technology or wish to collaborate, feel free to reach out to me through the following:

- **Channel**: [pd52261144](https://pd.qq.com/s/3edfwx2lm)
- **Email**: [405784684@qq.com]
- **Community**: [https://madong.tech](https://madong.tech)

#### Maintainer

[Mr.April](https://gitee.com/liu_guan_qing)
